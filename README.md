# Token ECA Alter

Token ECA alter allows you to alter the output of other tokens with ECA module.

## Usage

[token-eca:{existing token}#{key}]

[token-eca:{existing token}#{key}#{other_key}]

In ECA {key} used with Token Alter Event to identify by [token_alter_key], 
[token_alter_data] is the data of {existing token}.

After alter data use Token Alter: set data to set altered data.

## Exist key with module

__Truncate__:

Truncate data with default length 300 or define length option

```
[token-eca:node:title#truncate]
[token-eca:node:title#truncate{length:100}]
```

__Title case__

Uppercases the first letter of each word.

```
[token-eca:node:title#title-case]
```

__Uppercase__

Uppercases all characters.

```
[token-eca:node:title#uppercase]
```

__Lowwercase__

Lowercases all characters.

```
[token-eca:node:title#lowercase]
```

__first-uppercase__

Uppercase first characters.

```
[token-eca:node:title#first-uppercase]
```


__first-lowercase__

Lowercase first characters.

```
[token-eca:node:title#first-lowercase]
```

__base64-encode__

Base64 encode string.

```
[token-eca:node:title#base64-encode]
```

__base64-decode__

Base64 decode string.

```
[token-eca:node:title#base64-decode]
```

__md5__

Md5 hash string.

```
[token-eca:node:title#md5]
```

__sha1__

Sha1 hash string.

```
[token-eca:node:title#sha1]
```

__sha256__

Sha256 hash string.

```
[token-eca:node:title#sha256]
```

__url-encode__

URL encode string.

```
[token-eca:node:url#url-encode]
```

__url-decode__

URL decode string.

```
[token-eca:node:field-url#url-decode]
```

__Token alter chains__

```
[token-eca:node:title#lowercase#first-uppercase]
```
