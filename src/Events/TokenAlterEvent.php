<?php

namespace Drupal\token_eca_alter\Events;

use Drupal\Component\EventDispatcher\Event;

/**
 * The Token Alter Event.
 *
 * @package Drupal\token_eca_alter\Events
 */
class TokenAlterEvent extends Event {

  /**
   * Alter event.
   */
  const ALTER = 'token_eca_alter.eca_alter';

  /**
   * The data.
   *
   * @var string
   */
  protected string $data;

  /**
   * The token.
   *
   * @var string
   */
  protected string $token;

  /**
   * The key.
   *
   * @var string
   */
  protected string $key;

  /**
   * The event options.
   *
   * @var array
   */
  protected array $options;

  /**
   * EcaAlterEvent constructor.
   *
   * @param string $key
   *   The alter key.
   * @param string $token
   *   The token.
   * @param string $data
   *   The data.
   * @param array $options
   *   The options.
   */
  public function __construct(string $key, string $token, string $data, array $options = []) {
    $this->key = $key;
    $this->token = $token;
    $this->data = $data;
    $this->options = $options;
  }

  /**
   * Get alter key.
   *
   * @return string
   *   The alter key.
   */
  public function getKey():string {
    return $this->key;
  }

  /**
   * The token name.
   *
   * @return string
   *   The token name.
   */
  public function getToken():string {
    return $this->token;
  }

  /**
   * The token replaced data.
   *
   * @return string
   *   The token data.
   */
  public function getData():string {
    return $this->data;
  }

  /**
   * The token options data.
   *
   * @return array
   *   The token options.
   */
  public function getOptions():array {
    return $this->options;
  }

  /**
   * Set the data.
   *
   * @param string $data
   *   The data.
   */
  public function setData(string $data) {
    $this->data = $data;
  }

}
