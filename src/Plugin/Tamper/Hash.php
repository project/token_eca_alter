<?php

namespace Drupal\token_eca_alter\Plugin\Tamper;

use Drupal\Core\Form\FormStateInterface;
use Drupal\tamper\TamperableItemInterface;
use Drupal\tamper\TamperBase;

/**
 * Plugin implementation for hash string.
 *
 * @Tamper(
 *   id = "token_eca_alter_hash",
 *   label = @Translation("Hash String"),
 *   description = @Translation("Hash string."),
 *   category = "Text",
 *   handle_multiples = FALSE
 * )
 */
class Hash extends TamperBase {

  const SETTING_MODE = 'mode';

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    $config = parent::defaultConfiguration();
    $config[self::SETTING_MODE] = 'serialize';
    return $config;
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form[self::SETTING_MODE] = [
      '#type' => 'radios',
      '#title' => $this->t('Hash mode:'),
      '#options' => $this->getOptions(),
      '#default_value' => $this->getSetting(self::SETTING_MODE),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    parent::submitConfigurationForm($form, $form_state);
    $this->setConfiguration([self::SETTING_MODE => $form_state->getValue(self::SETTING_MODE)]);
  }

  /**
   * Get the encode / decode options.
   *
   * @return array
   *   List of options, keyed by method.
   */
  protected function getOptions() {
    return array_combine(hash_algos(), hash_algos());
  }

  /**
   * {@inheritdoc}
   */
  public function tamper($data, TamperableItemInterface $item = NULL) {
    $function = $this->getSetting(self::SETTING_MODE);
    return hash($function, $data);
  }

}
