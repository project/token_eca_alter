<?php

namespace Drupal\token_eca_alter\Plugin\Action;

use Drupal\Core\Form\FormStateInterface;
use Drupal\eca\Plugin\Action\ConfigurableActionBase;
use Drupal\token_eca_alter\Events\TokenAlterEvent;

/**
 * Action to store arbitrary value to memory store.
 *
 * @Action(
 *   id = "token_eca_alter_set_data",
 *   label = @Translation("Token Alter: set data"),
 *   description = @Translation("Set updated data to token")
 * )
 */
class SetDataAction extends ConfigurableActionBase {

  /**
   * {@inheritdoc}
   */
  public function execute(): void {
    $event = $this->getEvent();
    if (!$event || !($event instanceof TokenAlterEvent)) {
      return;
    }
    $token = $this->tokenService;
    $value = $this->configuration['value'];
    $value = (string) $token->replaceClear($value);
    $event->setData($value);
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration(): array {
    return [
      'value' => '',
    ] + parent::defaultConfiguration();
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state): array {
    $form = parent::buildConfigurationForm($form, $form_state);

    $form['value'] = [
      '#type'          => 'textarea',
      '#title'         => $this->t('The alter value for token'),
      '#default_value' => $this->configuration['value'],
      '#weight'        => -20,
      '#description'   => $this->t('The altered value for the token.'),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state): void {
    $this->configuration['value'] = $form_state->getValue('value');
    parent::submitConfigurationForm($form, $form_state);
  }

}
