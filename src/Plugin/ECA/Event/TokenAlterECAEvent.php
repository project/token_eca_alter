<?php

namespace Drupal\token_eca_alter\Plugin\ECA\Event;

use Drupal\Core\Form\FormStateInterface;
use Drupal\eca\Event\Tag;
use Drupal\eca\Plugin\ECA\Event\EventBase;
use Drupal\token_eca_alter\Events\TokenAlterEvent;

/**
 * Plugin implementation of the ECA Events for config.
 *
 * @EcaEvent(
 *   id = "token_eca_alter",
 *   deriver = "Drupal\token_eca_alter\Plugin\ECA\Event\TokenAlterECAEventDeriver"
 * )
 */
class TokenAlterECAEvent extends EventBase {

  /**
   * {@inheritdoc}
   */
  public static function definitions(): array {
    return [
      'callback' => [
        'label' => 'Token Alter',
        'event_name' => TokenAlterEvent::ALTER,
        'event_class' => TokenAlterEvent::class,
        'tags' => Tag::WRITE | Tag::READ,
      ],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state): array {
    if ($this->eventClass() === TokenAlterEvent::class) {
      $form['help'] = [
        '#type' => 'markup',
        '#markup' => $this->t('This event provides three tokens: <em>"[token_alter_key]"</em> the key to identify,<em>[token_alter_data]</em> the token data value, <em>"[token_alter_token]"</em> the token need alter <em>"[token_alter_options:*]"</em> the options.'),
        '#weight' => 10,
        '#description' => $this->t('This event provides three tokens: <em>"[token_alter_key]"</em> the key to identify,<em>[token_alter_data]</em> the token data value, <em>"[token_alter_token]"</em> the token need alter <em>"[token_alter_options:*]"</em> the options.'),
      ];
    }
    return $form;
  }

}
