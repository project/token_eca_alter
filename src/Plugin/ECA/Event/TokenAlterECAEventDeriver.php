<?php

namespace Drupal\token_eca_alter\Plugin\ECA\Event;

use Drupal\eca\Plugin\ECA\Event\EventDeriverBase;

/**
 * Deriver for token_eca_alter event plugins.
 */
class TokenAlterECAEventDeriver extends EventDeriverBase {

  /**
   * {@inheritdoc}
   */
  protected function definitions(): array {
    return TokenAlterECAEvent::definitions();
  }

}
