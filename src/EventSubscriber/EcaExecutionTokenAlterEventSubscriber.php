<?php

namespace Drupal\token_eca_alter\EventSubscriber;

use Drupal\eca\EcaEvents;
use Drupal\eca\Event\BeforeInitialExecutionEvent;
use Drupal\eca\EventSubscriber\EcaExecutionSubscriberBase;
use Drupal\token_eca_alter\Events\TokenAlterEvent;

/**
 * Adds the configuration to the Token service when executing ECA logic.
 */
class EcaExecutionTokenAlterEventSubscriber extends EcaExecutionSubscriberBase {

  /**
   * Subscriber method before initial execution.
   *
   * @param \Drupal\eca\Event\BeforeInitialExecutionEvent $before_event
   *   The according event.
   */
  public function onBeforeInitialExecution(BeforeInitialExecutionEvent $before_event): void {
    $event = $before_event->getEvent();
    if ($event instanceof TokenAlterEvent) {
      $this->tokenService->addTokenData('token_alter_key', $event->getKey());
      $this->tokenService->addTokenData('token_alter_data', $event->getData());
      $this->tokenService->addTokenData('token_alter_token', $event->getToken());
      $this->tokenService->addTokenData('token_alter_options', $event->getOptions());
    }
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents(): array {
    $events = [];
    $events[EcaEvents::BEFORE_INITIAL_EXECUTION][] = ['onBeforeInitialExecution'];
    return $events;
  }

}
