<?php

namespace Drupal\token_eca_alter\Service;

use Drupal\eca\Event\BaseHookHandler;

/**
 * Hook Handler for Token ECA Alter.
 *
 * @package Drupal\token_eca_alter\Service
 */
class HookHandler extends BaseHookHandler {

  /**
   * Alter the exist token.
   *
   * @param string $key
   *   The alter key.
   * @param string $token
   *   Exist token.
   * @param string $data
   *   The token data.
   * @param array $options
   *   The alter options.
   *
   * @return string
   *   Alter token result.
   */
  public function ecaAlter(string $key, string $token, string $data, array $options = []): string {
    /** @var \Drupal\token_eca_alter\Events\TokenAlterEvent $event */
    $event = $this->triggerEvent->dispatchFromPlugin('token_eca_alter:callback', $key, $token, $data, $options);
    if (empty($event)) {
      return '';
    }
    return $event->getData();
  }

}
