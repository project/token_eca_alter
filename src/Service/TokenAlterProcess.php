<?php

namespace Drupal\token_eca_alter\Service;

use Drupal\token\Token;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;

/**
 * The token alter process.
 *
 * @package Drupal\token_eca_alter\Service
 */
class TokenAlterProcess {


  /**
   * The event dispatcher.
   *
   * @var \Symfony\Component\EventDispatcher\EventDispatcherInterface
   */
  protected EventDispatcherInterface $eventDispatcher;

  /**
   * The token.
   *
   * @var \Drupal\token\Token
   */
  protected Token $token;

  /**
   * Set the dispatcher service.
   *
   * @param \Symfony\Component\EventDispatcher\EventDispatcherInterface $eventDispatcher
   *   The event dispatcher.
   */
  public function setDispatcher(EventDispatcherInterface $eventDispatcher) {
    $this->eventDispatcher = $eventDispatcher;
  }

  /**
   * Set the token.
   *
   * @param \Drupal\token\Token $token
   *   The token service.
   */
  public function setToken(Token $token) {
    $this->token = $token;
  }

  /**
   * Process the alter token service.
   *
   * @param string $name
   *   The token name.
   * @param array $data
   *   The token data.
   * @param array $options
   *   The token options.
   *
   * @return string
   *   Processed token.
   */
  public function processToken(string $name, array $data, array $options): string {
    if (!str_contains($name, '#')) {
      return $this->token->replace('[' . $name . ']', $data, $options + ['clear' => TRUE]);
    }
    $alters = explode('#', $name);
    $token = array_shift($alters);
    $token_data = $this->token->replace('[' . $token . ']', $data, $options + ['clear' => TRUE]);
    foreach ($alters as $alter_key) {
      $alter_options = [];
      $alter_id = $alter_key;
      if (strpos($alter_key, '}')) {
        $pos_s = strpos($alter_key, '{');
        $alter_id = substr($alter_key, 0, $pos_s);
        $alter_key = str_replace([$alter_id, '{', '}'], '', $alter_key);
        $pairs = explode(',', $alter_key);
        $alter_options = [];
        foreach ($pairs as $pair) {
          [$key, $value] = explode(':', $pair);
          $alter_options[trim($key)] = is_numeric($value) ? $value + 0 : $value;
        }
      }
      $token_data = _token_eca_alter_hook_handler()->ecaAlter($alter_id, $token, $token_data, $alter_options);
      ;
    }

    return $token_data;
  }

}
